-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: zetagas_db
-- ------------------------------------------------------
-- Server version	5.7.23-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `cell_phone` varchar(20) NOT NULL,
  `platform` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `joined` datetime(6) NOT NULL,
  `role` smallint(5) unsigned DEFAULT NULL,
  `photo` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2654 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `authtoken_token`
--

DROP TABLE IF EXISTS `authtoken_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `authtoken_token_user_id_35299eff_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_addresses`
--

DROP TABLE IF EXISTS `zetagas_cms_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `colony` varchar(200) NOT NULL,
  `street` varchar(200) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `num_int` varchar(10) NOT NULL,
  `num_ext` varchar(10) NOT NULL,
  `comments` longtext NOT NULL,
  `date` datetime(6) NOT NULL,
  `favorito` tinyint(1) NOT NULL,
  `citie_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_addresses_citie_id_ee5bfefb_fk_zetagas_cms_cities_id` (`citie_id`),
  KEY `zetagas_cms_addresses_usuario_id_5c76a9e1_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `zetagas_cms_addresses_citie_id_ee5bfefb_fk_zetagas_cms_cities_id` FOREIGN KEY (`citie_id`) REFERENCES `zetagas_cms_cities` (`id`),
  CONSTRAINT `zetagas_cms_addresses_usuario_id_5c76a9e1_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1479 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_choferes`
--

DROP TABLE IF EXISTS `zetagas_cms_choferes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_choferes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(70) NOT NULL,
  `last_name` varchar(70) NOT NULL,
  `cell_phone` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_cities`
--

DROP TABLE IF EXISTS `zetagas_cms_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_cities_state_id_0be4bb90_fk_zetagas_cms_states_id` (`state_id`),
  CONSTRAINT `zetagas_cms_cities_state_id_0be4bb90_fk_zetagas_cms_states_id` FOREIGN KEY (`state_id`) REFERENCES `zetagas_cms_states` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_devices`
--

DROP TABLE IF EXISTS `zetagas_cms_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `platform` varchar(20) NOT NULL,
  `token` longtext NOT NULL,
  `date` datetime(6) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_devices_usuario_id_f3b803e1_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `zetagas_cms_devices_usuario_id_f3b803e1_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_facturacion`
--

DROP TABLE IF EXISTS `zetagas_cms_facturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_facturacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(50) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `business_name` varchar(200) NOT NULL,
  `company` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_fact_address_id_325f224a_fk_zetagas_cms_addresses_id` (`address_id`),
  KEY `zetagas_cms_facturacion_usuario_id_14e2782c_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `zetagas_cms_fact_address_id_325f224a_fk_zetagas_cms_addresses_id` FOREIGN KEY (`address_id`) REFERENCES `zetagas_cms_addresses` (`id`),
  CONSTRAINT `zetagas_cms_facturacion_usuario_id_14e2782c_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_orders`
--

DROP TABLE IF EXISTS `zetagas_cms_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity_products` int(11) NOT NULL,
  `liters_in_pesos` varchar(10) NOT NULL,
  `date_order` varchar(20) NOT NULL,
  `hour_order` varchar(10) NOT NULL,
  `sha_order` longtext NOT NULL,
  `date_now` datetime(6) NOT NULL,
  `address_id` int(11) NOT NULL,
  `id_order_retation_pymentmethod_id` int(11) NOT NULL,
  `types_services_product_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `complete_order` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_orde_address_id_a56c1d50_fk_zetagas_cms_addresses_id` (`address_id`),
  KEY `b84018afa9085ac65fff9d07105af114` (`id_order_retation_pymentmethod_id`),
  KEY `D00b8b1f94675d83a5f2246e7838d52e` (`types_services_product_id`),
  KEY `zetagas_cms_orders_usuario_id_8187c0d2_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `D00b8b1f94675d83a5f2246e7838d52e` FOREIGN KEY (`types_services_product_id`) REFERENCES `zetagas_cms_typesservicesproducts` (`id`),
  CONSTRAINT `b84018afa9085ac65fff9d07105af114` FOREIGN KEY (`id_order_retation_pymentmethod_id`) REFERENCES `zetagas_cms_relationpaymentmethod` (`id`),
  CONSTRAINT `zetagas_cms_orde_address_id_a56c1d50_fk_zetagas_cms_addresses_id` FOREIGN KEY (`address_id`) REFERENCES `zetagas_cms_addresses` (`id`),
  CONSTRAINT `zetagas_cms_orders_usuario_id_8187c0d2_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_orderscompletekonecta`
--

DROP TABLE IF EXISTS `zetagas_cms_orderscompletekonecta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_orderscompletekonecta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_order_konecta` varchar(255) NOT NULL,
  `creation_date` varchar(255) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_orderstatus`
--

DROP TABLE IF EXISTS `zetagas_cms_orderstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_orderstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `color` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_paymentmethods`
--

DROP TABLE IF EXISTS `zetagas_cms_paymentmethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_paymentmethods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_premios`
--

DROP TABLE IF EXISTS `zetagas_cms_premios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_premios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  `title` varchar(180) NOT NULL,
  `points_kilos` varchar(15) NOT NULL,
  `points_liters` varchar(15) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `redimir` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_purchasesummary`
--

DROP TABLE IF EXISTS `zetagas_cms_purchasesummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_purchasesummary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_order` varchar(20) NOT NULL,
  `quantity_products` int(11) NOT NULL,
  `liters_in_pesos` varchar(10) NOT NULL,
  `date_order` varchar(20) NOT NULL,
  `hour_order` varchar(10) NOT NULL,
  `total_purchase` varchar(20) NOT NULL,
  `total_discount` varchar(20) NOT NULL,
  `total_final` varchar(20) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  `sha_order` longtext NOT NULL,
  `citie_id` int(11) NOT NULL,
  `id_order_retation_pymentmethod_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `relation_purchase_summary_user_id` int(11) NOT NULL,
  `types_services_product_id` int(11) NOT NULL,
  `relation_purchase_summary_telephone_operator_id` int(11) DEFAULT NULL,
  `relation_purchase_summary_ruta_and_chofer_id` int(11),
  `order_complete_konecta_id` int(11),
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_purchases_citie_id_998dba25_fk_zetagas_cms_cities_id` (`citie_id`),
  KEY `D267a6b2dbee106678835775af8dd20e` (`id_order_retation_pymentmethod_id`),
  KEY `zetagas_c_order_status_id_c0c463ad_fk_zetagas_cms_orderstatus_id` (`order_status_id`),
  KEY `e20f6a12cc86fa009103c461528cfab8` (`relation_purchase_summary_user_id`),
  KEY `D838d40a3c1eb164bc6939bc2d19f6dc` (`types_services_product_id`),
  KEY `af14679c5634075817a0ffd4c306bf2f` (`relation_purchase_summary_telephone_operator_id`),
  KEY `e4395fc4f88c5fae40ce97fb1b80d6d8` (`relation_purchase_summary_ruta_and_chofer_id`),
  KEY `D32c36d3b860baccbd9ef796797ef38a` (`order_complete_konecta_id`),
  CONSTRAINT `D267a6b2dbee106678835775af8dd20e` FOREIGN KEY (`id_order_retation_pymentmethod_id`) REFERENCES `zetagas_cms_relationpaymentmethod` (`id`),
  CONSTRAINT `D32c36d3b860baccbd9ef796797ef38a` FOREIGN KEY (`order_complete_konecta_id`) REFERENCES `zetagas_cms_orderscompletekonecta` (`id`),
  CONSTRAINT `D838d40a3c1eb164bc6939bc2d19f6dc` FOREIGN KEY (`types_services_product_id`) REFERENCES `zetagas_cms_typesservicesproducts` (`id`),
  CONSTRAINT `af14679c5634075817a0ffd4c306bf2f` FOREIGN KEY (`relation_purchase_summary_telephone_operator_id`) REFERENCES `zetagas_cms_relationpurchasesummarytelephoneoperators` (`id`),
  CONSTRAINT `e20f6a12cc86fa009103c461528cfab8` FOREIGN KEY (`relation_purchase_summary_user_id`) REFERENCES `zetagas_cms_relationpurchasesummaryuser` (`id`),
  CONSTRAINT `e4395fc4f88c5fae40ce97fb1b80d6d8` FOREIGN KEY (`relation_purchase_summary_ruta_and_chofer_id`) REFERENCES `zetagas_cms_relationpurchasesummaryrutaandchofer` (`id`),
  CONSTRAINT `zetagas_c_order_status_id_c0c463ad_fk_zetagas_cms_orderstatus_id` FOREIGN KEY (`order_status_id`) REFERENCES `zetagas_cms_orderstatus` (`id`),
  CONSTRAINT `zetagas_cms_purchases_citie_id_998dba25_fk_zetagas_cms_cities_id` FOREIGN KEY (`citie_id`) REFERENCES `zetagas_cms_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1944 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationpaymentmethod`
--

DROP TABLE IF EXISTS `zetagas_cms_relationpaymentmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationpaymentmethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_method_id` int(11) NOT NULL,
  `token` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zeta_payment_method_id_0ac0561e_fk_zetagas_cms_paymentmethods_id` (`payment_method_id`),
  CONSTRAINT `zeta_payment_method_id_0ac0561e_fk_zetagas_cms_paymentmethods_id` FOREIGN KEY (`payment_method_id`) REFERENCES `zetagas_cms_paymentmethods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationpurchasepremio`
--

DROP TABLE IF EXISTS `zetagas_cms_relationpurchasepremio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationpurchasepremio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(180) NOT NULL,
  `points_liters` varchar(15) NOT NULL,
  `points_kilos` varchar(15) NOT NULL,
  `image` varchar(100) NOT NULL,
  `email_user_atendio` varchar(254) NOT NULL,
  `email_user_atendido` varchar(254) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationpurchasesummaryrutaandchofer`
--

DROP TABLE IF EXISTS `zetagas_cms_relationpurchasesummaryrutaandchofer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationpurchasesummaryrutaandchofer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta_schedule` varchar(30) NOT NULL,
  `ruta_number_unit` varchar(20) NOT NULL,
  `ruta_model` varchar(20) NOT NULL,
  `ruta_anio` varchar(10) NOT NULL,
  `chofer_first_name` varchar(70) NOT NULL,
  `chofer_last_name` varchar(70) NOT NULL,
  `chofer_cell_phone` varchar(20) NOT NULL,
  `chofer_address` varchar(255) NOT NULL,
  `ruta_typeservice_id` int(11) NOT NULL,
  `chofer_indentificator` int(11),
  `ruta_indentificator` int(11),
  PRIMARY KEY (`id`),
  KEY `zet_ruta_typeservice_id_8d3b7290_fk_zetagas_cms_typesservices_id` (`ruta_typeservice_id`),
  CONSTRAINT `zet_ruta_typeservice_id_8d3b7290_fk_zetagas_cms_typesservices_id` FOREIGN KEY (`ruta_typeservice_id`) REFERENCES `zetagas_cms_typesservices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1417 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationpurchasesummarytelephoneoperators`
--

DROP TABLE IF EXISTS `zetagas_cms_relationpurchasesummarytelephoneoperators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationpurchasesummarytelephoneoperators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `cell_phone` varchar(20) NOT NULL,
  `sexo` varchar(30) NOT NULL,
  `age` varchar(3) NOT NULL,
  `address` varchar(255) NOT NULL,
  `start_time` varchar(30) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `citie_id` int(11),
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_relationp_citie_id_99f11b68_fk_zetagas_cms_cities_id` (`citie_id`),
  CONSTRAINT `zetagas_cms_relationp_citie_id_99f11b68_fk_zetagas_cms_cities_id` FOREIGN KEY (`citie_id`) REFERENCES `zetagas_cms_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1419 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationpurchasesummaryuser`
--

DROP TABLE IF EXISTS `zetagas_cms_relationpurchasesummaryuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationpurchasesummaryuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_user` varchar(150) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `colony` varchar(200) NOT NULL,
  `street` varchar(200) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  `num_int` varchar(10) NOT NULL,
  `num_ext` varchar(10) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `comments` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1944 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_relationrutachofer`
--

DROP TABLE IF EXISTS `zetagas_cms_relationrutachofer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_relationrutachofer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_now` datetime(6) NOT NULL,
  `chofer_id` int(11) NOT NULL,
  `ruta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_relati_chofer_id_45894a64_fk_zetagas_cms_choferes_id` (`chofer_id`),
  KEY `zetagas_cms_relationrut_ruta_id_0fd75454_fk_zetagas_cms_rutas_id` (`ruta_id`),
  CONSTRAINT `zetagas_cms_relati_chofer_id_45894a64_fk_zetagas_cms_choferes_id` FOREIGN KEY (`chofer_id`) REFERENCES `zetagas_cms_choferes` (`id`),
  CONSTRAINT `zetagas_cms_relationrut_ruta_id_0fd75454_fk_zetagas_cms_rutas_id` FOREIGN KEY (`ruta_id`) REFERENCES `zetagas_cms_rutas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_rutas`
--

DROP TABLE IF EXISTS `zetagas_cms_rutas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_rutas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule` varchar(30) NOT NULL,
  `number_unit` varchar(20) NOT NULL,
  `model` varchar(20) NOT NULL,
  `anio` varchar(10) NOT NULL,
  `date_now` datetime(6) NOT NULL,
  `citie_id` int(11) NOT NULL,
  `typeservice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_rutas_citie_id_53da6a9e_fk_zetagas_cms_cities_id` (`citie_id`),
  KEY `zetagas__typeservice_id_52e77247_fk_zetagas_cms_typesservices_id` (`typeservice_id`),
  CONSTRAINT `zetagas__typeservice_id_52e77247_fk_zetagas_cms_typesservices_id` FOREIGN KEY (`typeservice_id`) REFERENCES `zetagas_cms_typesservices` (`id`),
  CONSTRAINT `zetagas_cms_rutas_citie_id_53da6a9e_fk_zetagas_cms_cities_id` FOREIGN KEY (`citie_id`) REFERENCES `zetagas_cms_cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_states`
--

DROP TABLE IF EXISTS `zetagas_cms_states`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_telephoneoperators`
--

DROP TABLE IF EXISTS `zetagas_cms_telephoneoperators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_telephoneoperators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sexo` varchar(30) NOT NULL,
  `age` varchar(3) NOT NULL,
  `address` varchar(255) NOT NULL,
  `start_time` varchar(30) NOT NULL,
  `end_time` varchar(30) NOT NULL,
  `citie_id` int(11) NOT NULL,
  `usuario_id` int(11),
  PRIMARY KEY (`id`),
  KEY `zetagas_cms_telephone_citie_id_41d93bc0_fk_zetagas_cms_cities_id` (`citie_id`),
  KEY `zetagas_cms_telephoneoperato_usuario_id_03054281_fk_auth_user_id` (`usuario_id`),
  CONSTRAINT `zetagas_cms_telephone_citie_id_41d93bc0_fk_zetagas_cms_cities_id` FOREIGN KEY (`citie_id`) REFERENCES `zetagas_cms_cities` (`id`),
  CONSTRAINT `zetagas_cms_telephoneoperato_usuario_id_03054281_fk_auth_user_id` FOREIGN KEY (`usuario_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_typesservices`
--

DROP TABLE IF EXISTS `zetagas_cms_typesservices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_typesservices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `zetagas_cms_typesservicesproducts`
--

DROP TABLE IF EXISTS `zetagas_cms_typesservicesproducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zetagas_cms_typesservicesproducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` varchar(10) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `quantity_in` varchar(10) NOT NULL,
  `typeservice_id` int(11) NOT NULL,
  `citie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zetagas__typeservice_id_c05b6464_fk_zetagas_cms_typesservices_id` (`typeservice_id`),
  CONSTRAINT `zetagas__typeservice_id_c05b6464_fk_zetagas_cms_typesservices_id` FOREIGN KEY (`typeservice_id`) REFERENCES `zetagas_cms_typesservices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-18  9:30:27
